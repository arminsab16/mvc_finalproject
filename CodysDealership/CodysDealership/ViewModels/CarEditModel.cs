﻿using CodysDealership.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodysDealership.ViewModels
{
    public class CarEditModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid CarId { get; set; }

        public Guid MakeId { get; set; }

        public Guid BodyStyleId { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Model")]
        public string ModelName { get; set; }

        [Required]
        [StringLength(20)]
        public string Color { get; set; }

        [Required]
        [StringLength(20)]
        public string Price { get; set; }

        [Required]
        [StringLength(20)]
        public string Mileage { get; set; }

        [Required]
        [StringLength(20)]
        public string VIN { get; set; }

        [Required]
        [StringLength(20)]
        public string Drivetrain { get; set; }

        [Column(TypeName = "ntext")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        public bool Sold { get; set; }

        [Display(Name = "Image")]
        [StringLength(200, ErrorMessage = "The image name cannot exceed 200 characters.")]
        public string CarImage { get; set; }
        public HttpPostedFileBase CarImageFile { get; set; }

        //navagtion
        public virtual Make Make { get; set; }
        public virtual BodyStyle BodyStyle { get; set; }
        public virtual ICollection<Review> Review { get; set; }
    }
}