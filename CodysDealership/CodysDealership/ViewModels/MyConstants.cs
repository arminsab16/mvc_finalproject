﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodysDealership
{
    public class MyConstants
    {
        public const int ITEMS_PER_PAGE = 5;

        public const string BODYTYPE_IMAGE_PATH = "~/Content/BodyTypeImages/";
        public const string MAKE_IMAGE_PATH = "~/Content/MakeImages/";
        public const string CAR_IMAGE_PATH = "~/Content/CarImages/";
        public const string UPLOAD_IMAGE_PATH = "~/Content/Uploads/";

        public const int MAX_IMAGE_FILE_SIZE = 2097152;
        public const int THUMBNAIL_IMAGE_SIZE = 128;
        public const string THUMBNAIL_FOLDER_NAME = "Thumbnails/";

        public const int MAX_UPLOAD_IMAGE_WIDTH = 1024;
        public const int MAX_UPLOAD_IMAGE_HEIGHT = 768;

        public static readonly string[] ALLOWED_IMAGE_EXTENSIONS = new string[]
        {
            ".gif", ".png", ".jpeg", ".jpg"
        };
    }
}