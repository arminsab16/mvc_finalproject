﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodysDealership.Models;

namespace CodysDealership.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<BodyStyle> BodyStyles { get; set; }
        public IEnumerable<Make> Makes { get; set; }
    }
}