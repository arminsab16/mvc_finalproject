﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodysDealership.ViewModels
{
    public class MakeEditModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        [Display(Name = "#")]
        public Guid MakeId { get; set; }

        [Required]
        [Display(Name = "MakeName")]
        [StringLength(100)]
        public string MakeName { get; set; }

        [StringLength(100)]
        public string MakeImage { get; set; }
        public HttpPostedFileBase MakeImageFile { get; set; }
    }
}