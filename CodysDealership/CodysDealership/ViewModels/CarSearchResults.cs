﻿using CodysDealership.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PagedList;

namespace CodysDealership.ViewModels
{
    public class CarSearchResults
    {
        [Display(Name = "Search")]
        public string Search { get; set; }
        [Display(Name = "BodyStyleId")]
        public Guid? BodyStyleId { get; set; }
        [Display(Name = "Release Year")]
        public int? MinYear { get; set; }
        [Display(Name = "Release Year")]
        public int? MaxYear { get; set; }
        [Display(Name = "Make")]
        public Guid? MakeId { get; set; }

        public IEnumerable<BodyStyle> BodyStyles { get; set; }
        public IEnumerable<Make> Makes { get; set; }
        public IPagedList<Car> Results { get; set; }
    }
}