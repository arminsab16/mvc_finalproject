namespace CodysDealership.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Review
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ReviewId { get; set; }

        public Guid CarId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Column(TypeName = "ntext")]
        [MinLength(1)]
        [Required]
        public string Body { get; set; }

        [Required]
        //[Range(1, 5, ErrorMessage = "The Rating must be from {1} and {2}")]
        public byte Rating { get; set; }
    }
}
