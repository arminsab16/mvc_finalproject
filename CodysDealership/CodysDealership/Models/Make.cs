namespace CodysDealership.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Make")]
    public partial class Make
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        [Display(Name = "#")]
        public Guid MakeId { get; set; }

        [Required]
        [Display(Name = "MakeName")]
        [StringLength(100)]
        public string MakeName { get; set; }

        [StringLength(100)]
        public string MakeImage { get; set; }
    }
}
