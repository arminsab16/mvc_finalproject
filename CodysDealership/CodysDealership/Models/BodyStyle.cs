namespace CodysDealership.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BodyStyle")]
    public partial class BodyStyle
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid BodyStyleId { get; set; }

        [Required]
        [StringLength(100)]
        public string BodyType { get; set; }

        [StringLength(100)]
        public string BodyTypeImage { get; set; }
    }
}
