namespace CodysDealership.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CarImage")]
    public partial class CarImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ImageId { get; set; }

        public Guid CarId { get; set; }

        [Required]
        [StringLength(200)]
        public string ImageName { get; set; }

        [Required]
        [StringLength(50)]
        public string Side { get; set; }
    }
}
