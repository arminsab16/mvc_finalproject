namespace CodysDealership.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CodysDatabase : DbContext
    {
        public CodysDatabase()
            : base("name=CodysDatabase")
        {
        }

        public virtual DbSet<BodyStyle> BodyStyles { get; set; }
        public virtual DbSet<CarImage> CarImages { get; set; }
        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Make> Makes { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
