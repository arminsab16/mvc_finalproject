﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using CodysDealership.Models;
using CodysDealership.ViewModels;
using PagedList;

namespace CodysDealership.Controllers
{
    public class CarController : Controller
    {
        private CodysDatabase db = new CodysDatabase();

        // GET: Car
        public ActionResult Index(
            string search,
            Guid? bodystyleId,
            Guid? makeId,
            int? minYear,
            int? maxYear,
            int? page,
            int? itemsPerPage)
        {
            IQueryable<Car> cars = db.Cars.Include("BodyStyle");

            //filter the results
            if (bodystyleId != null)
            {
                cars = cars.Where(m => m.BodyStyleId == bodystyleId);
            }
            if (makeId != null)
            {
                cars = cars.Where(m => m.MakeId == makeId);
            }
            if (minYear != null)
            {
                cars = cars.Where(m => m.Year >= minYear);
            }
            if (maxYear != null)
            {
                cars = cars.Where(m => m.Year <= maxYear);
            }

            if (!string.IsNullOrWhiteSpace(search))
            {

                cars = cars.Where(
                    m => m.Year.ToString().Contains(search) ||
                         m.ModelName.Contains(search) ||
                         m.Make.MakeName.Contains(search)
                   );
            }

            cars = cars.OrderBy(m => m.ModelName);

            var model = new CarSearchResults
            {
                Search = search,
                BodyStyleId = bodystyleId,
                MakeId = makeId,
                MinYear = minYear,
                MaxYear = maxYear,
                BodyStyles = db.BodyStyles.OrderBy(g => g.BodyType).ToList(),
                Makes = db.Makes.OrderBy(m => m.MakeName).ToList(),
                Results = cars.ToPagedList(page ?? 1, itemsPerPage ?? 12),
            };
            return View("Index", model);
        }

        // GET: Car/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }

            return View(car);
        }

        // GET: Car/Create
        public ActionResult Create()
        {
            var makes = db.Makes.ToList();
            ViewBag.Makes = makes;

            var bodystyle = db.BodyStyles.ToList();
            ViewBag.Bodystyle = bodystyle;

            return View();
        }

        // POST: Car/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CarId,MakeId,BodyStyleId,Year,ModelName,Color,Price,Mileage,VIN,Drivetrain,Comments,Sold,CarImage")] Car car)
        {
            if (ModelState.IsValid)
            {
                db.Cars.Add(car);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var makes = db.Makes.ToList();
            ViewBag.Makes = makes;

            var bodystyle = db.BodyStyles.ToList();
            ViewBag.Bodystyle = bodystyle;

            return View(car);
        }

        // GET: Car/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // POST: Car/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CarEditModel viewModel)
        {
            //[Bind(Include = "CarId,MakeId,BodyStyleId,Year,ModelName,Color,Price,Mileage,VIN,Drivetrain,Comments,Sold")]Car car
            //if (ModelState.IsValid)
            //{
            //    db.Entry(car).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            //return View(car);
            var movieToUpdate = db.Cars.Where(x => x.CarId == viewModel.CarId).Single();

            //update the movie's properties
            string[] propertiesToUpdate = new string[]{
                "CarId",
                "MakeId",
                "BodyStyleId",
                "Year",
                "ModelName",
                "Color",
                "Price",
                "Mileage",
                "VIN",
                "Drivetrain",
                "Comments",
                "Sold",
                "CarImage",
            };
            if (TryUpdateModel(movieToUpdate, "", propertiesToUpdate))
            {
                //upload the movie image if needed
                if (ModelState.IsValid)
                {
                    //string fileName = viewModel.MovieImageFile.FileName;
                    //viewModel.MovieImage = fileName;
                    //movieToUpdate.MovieImage = fileName;

                    UploadCarImage(viewModel);
                }
                //update the movie's properties
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    return RedirectToAction("Index", "Car");
                }
            }

            return View(viewModel);
        }

        // GET: Car/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // POST: Car/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Car car = db.Cars.Find(id);
            db.Cars.Remove(car);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SubmitReview(Guid CarId, string reviewTitle, byte reviewRating, string reviewBody)
        {
            Car car = db.Cars.Find(CarId);
            if (car == null)
            {
                return HttpNotFound();
            }

            Review carRating = db.Reviews.Find(CarId);
            carRating = new Review
            {
                ReviewId = Guid.NewGuid(),
                CarId = CarId,
                Title = reviewTitle,
                Body = reviewBody,
                Rating = reviewRating
            };
            db.Reviews.Add(carRating);
            db.SaveChanges();
            return RedirectToAction("Details", "Car", new { id = CarId });
        }

        private void UploadImage(
            HttpPostedFileBase file,
            string destinationFolder,
            string filename,
            int maxWidth,
            int maxHeight)
        {
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > maxWidth || img.Height > maxWidth)
            {
                img.Resize(maxWidth, maxHeight);
            }
            img.Save(destinationFolder + filename);

            img.Resize(MyConstants.THUMBNAIL_IMAGE_SIZE, MyConstants.THUMBNAIL_IMAGE_SIZE);
            img.Save(destinationFolder + MyConstants.THUMBNAIL_FOLDER_NAME + filename);
        }

        private bool ValidateImageFile(string key, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError(key, "Please select a file.");
                return false;
            }
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(key, "File size was zero.");
                return false;
            }
            if (file.ContentLength >= MyConstants.MAX_IMAGE_FILE_SIZE)
            {
                ModelState.AddModelError(key, "File size was to big.");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!MyConstants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(key, $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        private void UploadCarImage(CarEditModel viewModel)
        {
            if (viewModel.CarImageFile != null &&
                ValidateImageFile("MovieImage", viewModel.CarImageFile))
            {
                try
                {
                    UploadImage(
                        file: viewModel.CarImageFile,
                        destinationFolder: MyConstants.CAR_IMAGE_PATH,
                        filename: viewModel.CarImage,
                        maxWidth: MyConstants.MAX_UPLOAD_IMAGE_WIDTH,
                        maxHeight: MyConstants.MAX_UPLOAD_IMAGE_HEIGHT);
                }
                catch (Exception)
                {
                    ModelState.AddModelError(
                        "CarImage",
                        "Sorry, an error occureed with uploading the image, please try again.");
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
