﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using CodysDealership.Models;
using CodysDealership.ViewModels;

namespace CodysDealership.Controllers
{
    public class MakeController : Controller
    {
        private CodysDatabase db = new CodysDatabase();

        // GET: Make
        public ActionResult Index()
        {
            return View(db.Makes.ToList());
        }

        // GET: Make/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Make make = db.Makes.Find(id);
            if (make == null)
            {
                return HttpNotFound();
            }
            return View(make);
        }

        // GET: Make/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Make/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MakeId,MakeName,MakeImage")] Make make)
        {
            if (ModelState.IsValid)
            {
                make.MakeId = Guid.NewGuid();
                db.Makes.Add(make);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(make);
        }

        // GET: Make/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Make make = db.Makes.Find(id);
            if (make == null)
            {
                return HttpNotFound();
            }
            return View(make);
        }

        // POST: Make/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MakeEditModel viewModel)
        {
            //if (ModelState.IsValid)
            //{
            //    db.Entry(make).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            //return View(make);
            var MakeToUpdate = db.Makes.Where(x => x.MakeId == viewModel.MakeId).Single();

            string[] propertiesToUpdate = new string[]{
                "MakeId",
                "MakeName",
                "MakeImage",
            };
            if (TryUpdateModel(MakeToUpdate, "", propertiesToUpdate))
            {
                //upload the movie image if needed
                if (ModelState.IsValid)
                {
                    //string fileName = viewModel.MovieImageFile.FileName;
                    //viewModel.MovieImage = fileName;
                    //movieToUpdate.MovieImage = fileName;

                    UploadMakeImage(viewModel);
                }
                //update the movie's properties
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    return RedirectToAction("Index", "Make");
                }
            }

            return View(viewModel);
        }

        private void UploadImage(
            HttpPostedFileBase file,
            string destinationFolder,
            string filename,
            int maxWidth,
            int maxHeight)
        {
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > maxWidth || img.Height > maxWidth)
            {
                img.Resize(maxWidth, maxHeight);
            }
            img.Save(destinationFolder + filename);

            img.Resize(MyConstants.THUMBNAIL_IMAGE_SIZE, MyConstants.THUMBNAIL_IMAGE_SIZE);
            img.Save(destinationFolder + MyConstants.THUMBNAIL_FOLDER_NAME + filename);
        }

        private bool ValidateImageFile(string key, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError(key, "Please select a file.");
                return false;
            }
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(key, "File size was zero.");
                return false;
            }
            if (file.ContentLength >= MyConstants.MAX_IMAGE_FILE_SIZE)
            {
                ModelState.AddModelError(key, "File size was to big.");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!MyConstants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(key, $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        private void UploadMakeImage(MakeEditModel viewModel)
        {
            if (viewModel.MakeImageFile != null &&
                ValidateImageFile("MakeImage", viewModel.MakeImageFile))
            {
                try
                {
                    UploadImage(
                        file: viewModel.MakeImageFile,
                        destinationFolder: MyConstants.MAKE_IMAGE_PATH,
                        filename: viewModel.MakeImage,
                        maxWidth: MyConstants.MAX_UPLOAD_IMAGE_WIDTH,
                        maxHeight: MyConstants.MAX_UPLOAD_IMAGE_HEIGHT);
                }
                catch (Exception)
                {
                    ModelState.AddModelError(
                        "MakeImage",
                        "Sorry, an error occureed with uploading the image, please try again.");
                }
            }
        }

        // GET: Make/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Make make = db.Makes.Find(id);
            if (make == null)
            {
                return HttpNotFound();
            }
            return View(make);
        }

        // POST: Make/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Make make = db.Makes.Find(id);
            db.Makes.Remove(make);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
