﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodysDealership.Models;

namespace CodysDealership.Controllers
{
    public class HomeController : Controller
    {
        private CodysDatabase _db = new CodysDatabase();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (disposing)
                {
                    _db.Dispose();
                    _db = null;
                }
                base.Dispose(disposing);
            }
        }

        public ActionResult Index()
        {
            var model = new ViewModels.HomeViewModel();

            //Body Types
            var bodyTypes =
                from b in _db.BodyStyles
                orderby b.BodyType
                select b;
            
            //var model = new ViewModels.HomeViewModel();
            model.BodyStyles = bodyTypes.ToList();

            //Makes
            var makes =
                from m in _db.Makes
                orderby m.MakeName
                select m;
            
            model.Makes = makes.ToList();

            return View("Index", model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}