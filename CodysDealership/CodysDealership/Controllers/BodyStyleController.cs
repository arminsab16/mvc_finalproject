﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CodysDealership.Models;

namespace CodysDealership.Controllers
{
    public class BodyStyleController : Controller
    {
        private CodysDatabase db = new CodysDatabase();

        // GET: BodyStyle
        public ActionResult Index()
        {
            return View(db.BodyStyles.ToList());
        }

        // GET: BodyStyle/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BodyStyle bodyStyle = db.BodyStyles.Find(id);
            if (bodyStyle == null)
            {
                return HttpNotFound();
            }
            return View(bodyStyle);
        }

        // GET: BodyStyle/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BodyStyle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BodyStyleId,BodyType,BodyTypeImage")] BodyStyle bodyStyle)
        {
            if (ModelState.IsValid)
            {
                bodyStyle.BodyStyleId = Guid.NewGuid();
                db.BodyStyles.Add(bodyStyle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bodyStyle);
        }

        // GET: BodyStyle/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BodyStyle bodyStyle = db.BodyStyles.Find(id);
            if (bodyStyle == null)
            {
                return HttpNotFound();
            }
            return View(bodyStyle);
        }

        // POST: BodyStyle/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BodyStyleId,BodyType,BodyTypeImage")] BodyStyle bodyStyle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bodyStyle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bodyStyle);
        }

        // GET: BodyStyle/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BodyStyle bodyStyle = db.BodyStyles.Find(id);
            if (bodyStyle == null)
            {
                return HttpNotFound();
            }
            return View(bodyStyle);
        }

        // POST: BodyStyle/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            BodyStyle bodyStyle = db.BodyStyles.Find(id);
            db.BodyStyles.Remove(bodyStyle);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
