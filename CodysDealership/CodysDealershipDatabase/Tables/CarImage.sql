﻿CREATE TABLE [dbo].[CarImage] (
    [ImageId]   uniqueidentifier NOT NULL default(newid()),
    [CarId]     uniqueidentifier  NOT NULL,
    [ImageName] NVARCHAR (200) NOT NULL,
    [Side]      NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_CartImage] PRIMARY KEY CLUSTERED ([ImageId] ASC),
	FOREIGN KEY ([CarId]) REFERENCES [dbo].[Cars] ([CarId]) ON DELETE CASCADE
);

