﻿CREATE TABLE [dbo].[Make] (
    [MakeId]   uniqueidentifier  NOT NULL default(newid()),
    [MakeName] NVARCHAR (100) NOT NULL,
	[MakeImage] NVARCHAR (100) NULL,
    CONSTRAINT [PK_Make] PRIMARY KEY CLUSTERED ([MakeId] ASC)
);

