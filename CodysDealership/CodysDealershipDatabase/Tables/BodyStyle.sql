﻿CREATE TABLE [dbo].[BodyStyle] (
    [BodyStyleId] uniqueidentifier NOT NULL default newid(),
    [BodyType]   NVARCHAR (100) NOT NULL,
	[BodyTypeImage] NVARCHAR (100) NULL,
    CONSTRAINT [PK_BodyStyle] PRIMARY KEY CLUSTERED ([BodyStyleId] ASC)
);

