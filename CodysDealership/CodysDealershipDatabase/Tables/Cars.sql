﻿CREATE TABLE [dbo].[Cars] (
    [CarId]       uniqueidentifier    NOT NULL default(newid()),
    [MakeId]      uniqueidentifier  NOT NULL,
    [BodyStyleId] uniqueidentifier   NOT NULL,
    [Year]        int   NOT NULL,
    [ModelName]   NVARCHAR (100) NOT NULL,
    [Color]       NVARCHAR (20)  NOT NULL,
    [Price]       NVARCHAR (20)  NOT NULL,
    [Mileage]     NVARCHAR (20)  NOT NULL,
    [VIN]         NVARCHAR (20)  NOT NULL,
    [Drivetrain]  NVARCHAR (20)  NOT NULL,
    [Comments]    NTEXT          NULL,
    [Sold]        BIT            NOT NULL,
	[CarImage]	  NVARCHAR(200)  NULL,
    CONSTRAINT [PK_Cars] PRIMARY KEY CLUSTERED ([CarId] ASC),
	FOREIGN KEY ([BodyStyleId]) REFERENCES [dbo].[BodyStyle] ([BodyStyleId]) ON DELETE CASCADE,
	FOREIGN KEY ([MakeId]) REFERENCES [dbo].[Make] ([MakeId]) ON DELETE CASCADE
);

