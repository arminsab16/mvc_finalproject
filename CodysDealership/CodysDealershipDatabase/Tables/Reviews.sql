﻿CREATE TABLE [dbo].[Reviews] (
    [ReviewId] uniqueidentifier NOT NULL default(newid()),
    [CarId]   uniqueidentifier            NOT NULL,
    [Title]    NVARCHAR (100) NOT NULL,
    [Body]     NTEXT          NOT NULL,
	[Rating]   tinyint		  NOT NULL,
    CONSTRAINT [PK_Reviews] PRIMARY KEY CLUSTERED ([ReviewId] ASC),
	FOREIGN KEY ([CarId]) REFERENCES [dbo].[Cars] ([CarId]) ON DELETE CASCADE
);

